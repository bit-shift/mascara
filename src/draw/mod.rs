/**
 * This file is part of mascara.
 *
 * Copyright (C) 2016 by Erik Kundt <bitshift@posteo.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

extern crate svg;

pub mod generate;

use self::svg::Document;
use self::svg::node::element::Path;
use self::svg::node::element::path::Data;

pub struct SVG {
    pub filename:   String,
}

pub struct GTKWindow {

}

pub trait Drawer {
    fn draw(&self);
}

impl Drawer for SVG {
    fn draw(&self) {
        let data = Data::new()
                        .move_to((10, 10))
                        .line_by((0, 50))
                        .line_by((50, 0))
                        .line_by((0, -50))
                        .close();

        let path = Path::new()
                        .set("fill", "none")
                        .set("stroke", "black")
                        .set("stroke-width", 3)
                        .set("d", data);

        let document = Document::new()
                                .set("viewBox", (0, 0, 70, 70))
                                .add(path);

        svg::save(&self.filename, &document).unwrap();
    }
}

impl Drawer for GTKWindow {
    fn draw(&self) {

    }
}
